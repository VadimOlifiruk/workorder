import { LightningElement, api } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class orderStepOne extends LightningElement {

    status;
    priority;    
    workType;
    subject;
    description;

    statusChangedHandler(event){
        this.status = event.target.value;  
    }

    priorityChangedHandler(event){
        this.priority = event.target.value;  
    }

    workTypeChangedHandler(event){
        this.workType = event.target.value;  
    }

    subjectChangedHandler(event){
        this.subject = event.target.value;  
    }

    descriptionChangedHandler(event){
        this.description = event.target.value;  
    }

    createWorkOrder() {

        let isvalid = true;

        this.template.querySelectorAll('lightning-input').forEach(element => {            
            element.reportValidity();
            isvalid = isvalid && element.checkValidity();
        });


        if (isvalid) {  

            let fields = {'Status': this.status, 'Priority': this.priority, 'WorkTypeId' : this.workType, 'Subject' : this.subject, 'Description' : this.description};
            let objRecordInput = {'apiName' : 'WorkOrder', fields};
            
            createRecord(objRecordInput).then(response => {          

                const e = new CustomEvent('stepone', {
                  
                    detail: {result:"true", order: response.id, type: this.workType}
                    });
                  
                    this.dispatchEvent(e);    
    
    
            }).catch(error => {

                const evt = new ShowToastEvent({   
                    variant: error,                     
                    message: error.body.message                   
                });

                this.dispatchEvent(evt);
            });            
        }
    }
}