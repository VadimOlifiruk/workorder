import { LightningElement, api } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class orderStepTwo extends LightningElement {
    
    status;   
    @api workOrder;
    @api workType;
    description;    

    statusChangedHandler(event){
        this.status = event.target.value;  
    }   

    workOrderChangedHandler(event){
        this.workOrder = event.target.value;  
    }

    workTypeChangedHandler(event){
        this.workType = event.target.value;  
    }

    descriptionChangedHandler(event){
        this.description = event.target.value;  
    }

    createLineItem() {

        let isValid = true;

        this.template.querySelectorAll('lightning-input').forEach(element => {            
            element.reportValidity();
            isValid = element.checkValidity() && isValid;
        });

        if (isValid) {  

            let fields = {'Status': this.status, 'WorkOrderId': this.workOrder, 'WorkTypeId' : this.workType, 'Description' : this.description};
            let objRecordInput = {'apiName' : 'WorkOrderLineItem', fields};

            createRecord(objRecordInput).then(response => {         


                const evtSuccess = new ShowToastEvent({                                        
                    message: `Record ${response.id} added`                   
                });

                this.dispatchEvent(evtSuccess); 
    
    
            }).catch(error => {

                const evt = new ShowToastEvent({  
                    message: error.body.message                   
                });

                this.dispatchEvent(evt);
            });            
        }
    }
}