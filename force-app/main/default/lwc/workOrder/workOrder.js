import { LightningElement, track} from 'lwc';

export default class WorkOrder extends LightningElement {

    @track showstepOne = true;
    @track showstepTwo = false;

    @track workTypeId;
    @track workOrderId;

    
    handleStepOne(event) {

        if (event.detail.result) {            

            this.workTypeId = event.detail.type;
            this.workOrderId = event.detail.order;

            this.showstepOne = false;
            this.showstepTwo = true;
        }        
    }   
}